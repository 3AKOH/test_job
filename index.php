<?php
/**
 * Получить первое - не пустое значение
 * @param $arr
 * @return bool|string
 */
function setFirst($arr){
    if(!is_array($arr)) return false;
    $count = count($arr);
    if($count<1) return '';
    for($i=0;$i<$count;$i++){
        if(!empty($arr[$i])){
            return trim($arr[$i]);
            break;
        }
    }
}

/**
 * ЗАДАНИЕ №1
 *
 * Распарсить sms сообщение
 * @param $sms
 * @return array|bool
 */
function smsParse($sms){
    if(empty($sms) || !is_string($sms)) return false;       //проверка входных данных
    if(preg_match_all('/[\D](\d{4,5}[^р,\d])|(\d{13,15})|(\d+[,]?\d+?р.)/',$sms,$found)){
        $purse = setFirst($found[3]);
        return array('code'=>setFirst($found[1]),'account'=>setFirst($found[2]),'purse'=>mb_substr($purse,0,strlen($purse)-3));
    }else{
        return false;
    }
}

/**
 * Очистить строку
 * @param $str
 * @return string
 */
function clearString($str){
    if(!is_string($str)) return '';
    else return trim(strip_tags($str));
}

/**
 * Обрабатываем POST на парсинг смс
 * Формируем HTML и дописываем в массив
 * Отправляем JSON
 */
if(isset($_POST['sms'])){
    $sms = clearString($_POST['sms']);
    if(!empty($sms)){
        $res = smsParse($sms);
        $res['html'] = '<p>Код: <b>'.$res['code'].'</b></p><p>Сумма: <b>'.$res['purse'].'</b></p><p>Кошелек: <b>'.$res['account'].'</b></p>';
        echo json_encode($res,1);
    }else{
        echo '';
    }
    exit;
}
?>


<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Тестовое задание</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">
    <style>
        body{
            padding: 40px;
        }
        h2{
            font-size: 1.4rem;
            margin-bottom: 40px;
        }
        textarea{
            padding: 20px 0;
        }
    </style>
</head>
<body class="rel">
<h1>#1 и #2 Задание:</h1>
<h2>Введите текст из смс и нажмите "Парсить".</h2>
<textarea name="sms" id="sms" cols="60" rows="5">
    Пароль: 1998
    Спишется 44,23р.
    Перевод на счет 410013501287406
</textarea>
<p><button class="btn btn-success" data-toggle="modal" data-target="#myModal">Парсить</button></p>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/tether/1.3.1/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js" integrity="sha384-vZ2WRJMwsjRMW/8U7i6PWi6AlO1L79snBrmgiDpgIWJ82z8eA5lenwvxbMV1PAh7" crossorigin="anonymous"></script>
<script src="js/index.js"></script>


<!-- Модальное окно -->
<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Результат запроса</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Закрыть окно</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>
