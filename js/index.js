$(document).ready(function(){
    $('#myModal').on('shown.bs.modal', function (e) {
        var modal = $(this);
        var id = $(this).attr('id');
        $.ajax({
            type: "POST",
            url: '/',
            data: {sms:$('#sms').val()},
            success: function(data){
                if(data != undefined && data != ''){
                    var data = JSON.parse(data);
                    console.log(data);
                    modal.find('.modal-body').html(data.html);
                }else{
                    modal.find('.modal-body').html('<p>Ошибка... Вы отправили пустую строку.</p>');
                }
            },
            error: function(){
                alert('Error!');
            }
        });
    })
});